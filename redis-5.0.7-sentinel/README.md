role: redis-cluster-3sentinel-1m2s

使用方法： 
1. 新建inventory ,redis.inventory  

[redis]  
10.4.0.37  #master
10.4.0.43  #slave1
10.4.0.80  #slave2
  
  
2. 将 redis-cluster-3sentinel-1m2s/tasks/main.yml 文件中的 master ,slave1 ,slave2的IP地址替换为相应的IP
3. 运行playbook
$ansible-playbook -i redis.inventory redis-cluster-3sentinel-1m2s.yaml  -e "master=10.4.0.37"  -e"slave1=10.4.0.43" -e"slave2=10.4.0.80"