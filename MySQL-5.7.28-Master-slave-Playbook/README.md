参考的https://github.com/xiaoyaolaotou/Ansible-Mysql  优化了蛮多，但是思路最重要，所以把出处写出来，感谢

使用方法
要求: ansible >= 2.9 ,不然mysql_replication会出错
      把mysql的bin文件下载下来放在common role的file目录下(路径: roles/common/files/mysql.zip) ,下载的地址: 
      
      链接：https://pan.baidu.com/s/1WCgOQRQFnz7rh4UdBI4i7g 
      提取码：it8k
1. 编辑 hosts文件，将master，slave IP地址填如，Slave可以是多个
2. 编辑 vars.yaml文件
3. 运行playbook 
#ansible-playbook -i hosts mysql_cluster.yaml -e"@vars.yaml"
4. 验证mysql 的主从